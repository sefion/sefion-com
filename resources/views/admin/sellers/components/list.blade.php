<div class="col-xs-12">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>                      
                        <th>{{ trans('labels.Name') }}</th>
                        <th>{{ trans('labels.Image') }}</th>
                        <th>{{ trans('labels.OtherInfo') }}</th>
                        <th>{{ trans('labels.Action') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($manufacturers)>0)
                    @foreach ($manufacturers  as $key=>$manufacturer)
                        <tr>
                            <td>{{ $manufacturer->id }}</td>
                            <td>{{ $manufacturer->name }}</td>
                            <td><img src="{{asset('').'/'.$manufacturer->image}}" alt="" width=" 100px"></td>
                            <td>
                            	<!--<strong>{{ trans('labels.ClickDate') }}: </strong> {{ $manufacturer->clik_date }}<br>-->
                                <strong>{{ trans('labels.URL') }}: </strong>{{ $manufacturer->url }} <br>
                                <!--<strong>{{ trans('labels.Clicked') }}: </strong>{{ $manufacturer->url_clicked }}-->  
                            </td>
                            <td>
                            	<a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editmanufacturer/{{ $manufacturer->id }}" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                                <a id="manufacturerFrom" manufacturers_id='{{ $manufacturer->id }}' data-toggle="tooltip" data-placement="bottom" title="Delete" href="#" class="badge bg-red"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                    
                    @else
                       <tr>
                            <td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                	{{$manufacturers->links('vendor.pagination.default')}}
                </div>
              </div>