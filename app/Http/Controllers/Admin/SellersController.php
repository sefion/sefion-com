<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;
use App;
use Lang;

use App\Admin;

use DB;
//for password encryption or hash protected
use Hash;
//use App\Administrator;

//for authenitcate login data
use Auth;
use Session;
//for requesting a value 
use Illuminate\Http\Request;

use App\Models\Orders;
use App\Models\Sellers;

class SellersController extends Controller {
    public function sellerList(Request $request) {

        $view = 'admin.sellers';        

        $title = array();		
        
        $sellers = Sellers::paginate(20);
        $pageTitle = Lang::get("labels.Sellers");

        $data = compact('pageTitle', 'sellers');   
        
        if($request->isMethod('post')) {
            $data = view('admin.sellers.components.list', $data)->render();            

            return response()->json($data);
        }

        return view($view, $data);
		
    }
}