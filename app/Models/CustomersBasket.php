<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomersBasket extends Model
{
    protected $table = "customers_basket";
}
